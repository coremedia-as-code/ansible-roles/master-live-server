# CoreMedia - master-live-server

## Ports

```
40280
40283
46278
40298
40299
40209
40205
```

## dependent services

### database
```
master_live:
  sql:
    store:
      url: 'jdbc:mysql://backend_database.int:3306/cm_master'
      user: 'cm_master'
      password: 'cm_master'
```
