import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/etc/ansible/facts.d",
    "/opt/coremedia/master-live-server/server-lib",
    "/opt/coremedia/master-live-server/common-lib",
    "/opt/coremedia/master-live-server/current/webapps/master-live-server/WEB-INF/properties/corem"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/ansible/facts.d/master-live-server.fact",
    "/opt/coremedia/master-live-server/common-lib/coremedia-tomcat.jar",
    "/opt/coremedia/master-live-server/current/bin/setenv.sh",
    "/opt/coremedia/master-live-server/master-live-server.properties"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.user("coremedia").exists
    assert host.user("master-live-server").exists
    assert host.group("coremedia").exists
